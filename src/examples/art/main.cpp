#include <cmath>
#include <cstdlib>
#include <ctime>
#include <initializer_list>
#include <memory>
#include <string>

#include <yannpp/common/array3d.h>
#include <yannpp/common/array3d_math.h>
#include <yannpp/layers/crossentropyoutputlayer.h>
#include <yannpp/layers/fullyconnectedlayer.h>
#include <yannpp/network/activator.h>
#include <yannpp/network/network2.h>

#include "bitmap_image.hpp"

float tanh_one(float x)
{
    const float ex = exp(x);
    const float emx = exp(-x);
    return (ex - emx) / (ex + emx);
}

std::shared_ptr<yannpp::fully_connected_layer_t<float>> layer(size_t in,
                                                              size_t out,
                                                              const yannpp::activator_t<float> &a)
{
    using namespace yannpp;
    auto result = std::make_shared<yannpp::fully_connected_layer_t<float>>(in, out, a);
    result->load({array3d_t<float>(shape3d_t(out, in, 1), 0.0f, 1.0f)},
                 {array3d_t<float>(shape_row(out), 0.0f, 1.0f)});
    return result;
}

int main(int argc, char *argv[])
{
    std::srand(std::time(NULL));

    using namespace yannpp;
    activator_t<float> tanh_activator(
        [](const array3d_t<float> &x) {
            array3d_t<float> result(x);
            result.apply(tanh_one);
            return result;
        },
        [](array3d_t<float> const &x) { return x; });
    activator_t<float> sigmoid_activator(sigmoid_v<float>, sigmoid_derivative_v<float>);
    network2_t<float> network(
        std::initializer_list<network2_t<float>::layer_type>({layer(5, 15, tanh_activator),
                                                              layer(15, 15, tanh_activator),
                                                              layer(15, 15, tanh_activator),
                                                              layer(15, 15, tanh_activator),
                                                              layer(15, 15, tanh_activator),
                                                              layer(15, 15, tanh_activator),
                                                              layer(15, 3, sigmoid_activator)}));

    network.init_layers();

    const int width = 320;
    const int height = 320;
    bitmap_image img(height, width);

    const float z1 = rand() / RAND_MAX;
    const float z2 = -rand() / RAND_MAX;

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            const float x = i / (height + 0.0f) - 0.5f;
            const float y = j / (width + 0.0f) - 0.5f;
            const float R = std::sqrt(x * x + y * y);
            std::vector<float> v = {x, y, R, z1, z2};

            array3d_t<float> input(std::move(v));
            array3d_t<float> result = network.feedforward(input);

            const uint8_t r = 255 * result.at(0, 0, 0);
            const uint8_t g = 255 * result.at(1, 0, 0);
            const uint8_t b = 255 * result.at(2, 0, 0);
            img.set_pixel(i, j, r, g, b);
        }
    }

    img.save_image("test.bmp");
    return 0;
}
